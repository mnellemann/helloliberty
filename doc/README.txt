To setup as a system service, copy the docker.helloliberty.service
file into /etc/systemd/system/ and enable the service. Remember to
change the path to the helloliberty.war file and ports accordingly.

systemctl enable docker.helloliberty
