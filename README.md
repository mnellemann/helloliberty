# Hello Liberty

Test liberty web application.

## Usage

Download the application:

```
wget https://bitbucket.org/mnellemann/helloliberty/downloads/helloliberty.war
```

### Method 1 - build an application container


```
cat << EOF > Dockerfile
FROM websphere-liberty:webProfile8
COPY helloliberty.war /config/dropins/helloliberty.war
EOF

docker build -t helloliberty .


docker run --name hello1 -p 9080:9080 helloliberty
```


### Method 2 - inject war file with volume


```
docker run --name hello2 -p 9080:9080 -p 9443:9443 \
    -v $(pwd)/helloliberty.war:/config/dropins/helloliberty.war \
    websphere-liberty:webProfile8
```



### Test with browser

Connect to the container on the specified port:

<http://localhost:9080/helloliberty/>

