package biz.nellemann.helloliberty;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biz.nellemann.helloliberty.model.Person;

 @WebServlet(urlPatterns="/search")
 public class SearchServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private static ArrayList<Person> people = new ArrayList<>();

    @Override
    public void init() {
       people.add(new Person("Scrooge", "McDuck", "scrooge@test.com"));
       people.add(new Person("Donald", "Duck", "donald@test.com"));
       people.add(new Person("Gladstone", "Gander", "Gladstone@test.com"));
       people.add(new Person("John D.", "Rockerduck", "John@test.com"));
       people.add(new Person("Katie", "Mallard", "Katie@test.com"));
       people.add(new Person("Grand", "Mogul", "Grand@test.com"));
       people.add(new Person("Velma", "Vanderduck", "Velma@test.com"));
       people.add(new Person("Panchito", "Pistoles", "Panchito@test.com"));
       people.add(new Person("Gyro", "Gearloose", "Gyro@test.com"));
       people.add(new Person("Jubal", "Pomp", "Jubal@test.com"));
       people.add(new Person("Emil", "Eagle", "Emil@test.com"));
       people.add(new Person("Garvey", "Gull", "Garvey@test.com"));
       people.add(new Person("Albert", "Quackmore", "Albert@test.com"));
    }


    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
    */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.getWriter().append(request.toString());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
    */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String query = request.getParameter("search");

        List<String> results = new ArrayList<>();
        if(!query.isEmpty()) {
            for(Person p : people) {
                if(p.search(query)) {
                    results.add(p.toString());
                }
            }
        }

        PrintWriter writer = response.getWriter();
        for(String s : results) {
            writer.append(s);
        }

    }

}

