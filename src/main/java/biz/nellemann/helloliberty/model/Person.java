package biz.nellemann.helloliberty.model;

public class Person {

    private String firstName;
    private String lastName;
    private String email;

    public Person(String first, String last, String email) {
        this.firstName = first;
        this.lastName = last;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return String.format("<tr><td>%s</td><td>%s</td><td>%s</td></tr>", firstName, lastName, email);
    }

    public boolean search(String s) {
        String q = s.toLowerCase();

        if(firstName.toLowerCase().matches("(.*)"+q+"(.*)")) {
            return true;
        }

        if(lastName.toLowerCase().matches("(.*)"+q+"(.*)")) {
            return true;
        }

        return false;
    }
}
